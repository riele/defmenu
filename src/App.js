import './App.css';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import AppMenu from './components/menu/AppMenu';
import { Atividades, Bairros, Cidades, Funcionarios, Home, Logradouros, Regioes, RelatorioClientes, RelatorioOSGeral, RelatorioOSPRocedimento } from './pages/pages';

function App() {
  return (
    <BrowserRouter>
      <div className='App'>
        <AppMenu />
        <h1><Link to='/'>MY APP</Link></h1>
      </div>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/funcionarios' element={<Funcionarios />} />
        <Route path='/regioes' element={<Regioes />} />
        <Route path='/logradouros' element={<Logradouros />} />
        <Route path='/bairros' element={<Bairros />} />
        <Route path='/cidades' element={<Cidades />} />
        <Route path='/atividades' element={<Atividades />} />
        <Route path='/relatorio-os-geral' element={<RelatorioOSGeral />} />
        <Route path='/relatorio-os-procedimento' element={<RelatorioOSPRocedimento />} />
        <Route path='/relatorio-clientes' element={<RelatorioClientes />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
