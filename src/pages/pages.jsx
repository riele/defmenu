import React from 'react'

export const Home = () => (
  <div className="page">
    <h1>Home</h1>
  </div>
)

export const Funcionarios = () => (
  <div className="page">
    <h1>Funcionários</h1>
  </div>
)

export const Regioes = () => (
  <div className="page">
    <h1>Regiões</h1>
  </div>
)

export const Bairros = () => (
  <div className="page">
    <h1>Bairros</h1>
  </div>
)

export const Cidades = () => (
  <div className="page">
    <h1>Cidades</h1>
  </div>
)

export const Atividades = () => (
  <div className="page">
    <h1>Atividades</h1>
  </div>
)

export const Logradouros = () => (
  <div className="page">
    <h1>Logradouros</h1>
  </div>
)

export const RelatorioOSGeral = () => (
  <div className="page">
    <h1>Relatório Geral de OS</h1>
  </div>
)

export const RelatorioOSPRocedimento = () => (
  <div className="page">
    <h1>Relatório de procedimentos de OS</h1>
  </div>
)

export const RelatorioClientes = () => (
  <div className="page">
    <h1>Relatório de clientes</h1>
  </div>
)