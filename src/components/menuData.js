export const menuData = [
  {
    "cod": 1,
    "rotulo": "Cadastros",
    "rota": null,
    "filhos": [
      {
        "cod": 19,
        "rotulo": "Funcionários",
        "rota": "/funcionarios",
        "filhos": []
      },
      {
        "cod": 20,
        "rotulo": "Regiões",
        "rota": "/regioes",
        "filhos": []
      },
      {
        "cod": 21,
        "rotulo": "Logradouros",
        "rota": "/logradouros",
        "filhos": []
      },
      {
        "cod": 22,
        "rotulo": "Bairros",
        "rota": "/bairros",
        "filhos": []
      },
      {
        "cod": 23,
        "rotulo": "Cidades",
        "rota": "/cidades",
        "filhos": []
      },
      {
        "cod": 29,
        "rotulo": "Atividades",
        "rota": "/atividades",
        "filhos": []
      }
    ]
  },
  {
    "cod": 2,
    "rotulo": "Clientes",
    "rota": null,
    "filhos": [
      {
        "cod": 31,
        "rotulo": "Cadastro",
        "rota": "/clientes",
        "filhos": []
      }
    ]
  },
  {
    "cod": 3,
    "rotulo": "Ordem de serviço",
    "rota": null,
    "filhos": [
      {
        "cod": 36,
        "rotulo": "Incluir",
        "rota": "/os",
        "filhos": []
      },
      {
        "cod": 40,
        "rotulo": "Encerrar procedimento",
        "rota": "/encerrar-proc",
        "filhos": []
      }
    ]
  },
  {
    "cod": 4,
    "rotulo": "Relatórios",
    "rota": null,
    "filhos": [
      {
        "cod": 5,
        "rotulo": "Ordem de serviço",
        "rota": null,
        "filhos": [
          {
            "cod": 56,
            "rotulo": "Geral",
            "rota": "/relatorio-os-geral",
            "filhos": []
          },
          {
            "cod": 212,
            "rotulo": "Procedimentos",
            "rota": "/relatorios-os-procedimento",
            "filhos": []
          }
        ]
      },
      {
        "cod": 55,
        "rotulo": "Clientes",
        "rota": "/relatorio-clientes",
        "filhos": []
      }
    ]
  }
]