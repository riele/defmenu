import './Overlay.css'
import React from 'react'

const Overlay = props => {
  return (
    <div className='overlay' onClick={props.click} />
  )
}

export default Overlay
