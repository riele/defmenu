import './MenuItem.css'
import React from 'react'
import { Link } from 'react-router-dom'

const MenuItem = props => {
  return (
    <Link className='menuitem' style={props.style} to={props.item.rota} onClick={props.click}>{props.item.rotulo}</Link>
  )
}

export default MenuItem
