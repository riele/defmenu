import React, { useState } from 'react'
import { IconContext } from 'react-icons'
import HamburgerButton from './HamburgerButton/HamburgerButton'
import Sidebar from './Sidebar/Sidebar'
import CloseMenuButton from './CloseMenuButton/CloseMenuButton'
import Overlay from './Overlay/Overlay'

const AppMenu = () => {
  const [menuOpen, setMenuOpen] = useState(false)

  const handleClick = () => setMenuOpen(!menuOpen)

  return (
    <>
      <IconContext.Provider value={{}}>
        <HamburgerButton click={handleClick} />
        {menuOpen && (
          <>
            <Sidebar click={handleClick} />
            <Overlay click={handleClick} />
            <CloseMenuButton click={handleClick} />
          </>
        )}
      </IconContext.Provider>
    </>
  )
}

export default AppMenu
