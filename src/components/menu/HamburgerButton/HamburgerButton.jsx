import './HamburgerButton.css'
import React from 'react'
import * as FaIcons from 'react-icons/fa'

const HamburgerButton = props => {
  return (
    <div className='hamburger-btn'>
      <FaIcons.FaBars onClick={props.click} />
    </div>
  )
}

export default HamburgerButton
