import './CloseMenuButton.css'
import React from 'react'
import { IoIosClose } from 'react-icons/io'

const CloseMenuButton = props => {
  return (
    <div className='closeMenuBtn' onClick={props.click}>
      <IoIosClose />
    </div>
  )
}

export default CloseMenuButton
