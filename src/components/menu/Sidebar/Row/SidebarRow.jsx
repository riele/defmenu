import './SidebarRow.css'
import React, { useState } from 'react'
import { FaChevronDown, FaChevronRight } from 'react-icons/fa'
import MenuItem from '../../MenuItem/MenuItem'

const SidebarRow = props => {
  const [isOpen, setIsOpen] = useState(false)

  const handleClick = () => setIsOpen(!isOpen)

  return (
    <div className='sidebarrow'>
      <div className='menuheader' style={props.style} onClick={handleClick}>
        {props.entry.rotulo}
        {isOpen
          ? <FaChevronDown color='#000' />
          : <FaChevronRight color='#000' />}
      </div>
      <div className={`submenu ${isOpen ? 'visible' : ''}`}>
        {props.entry.filhos.map(entry => (
          entry.filhos.length > 0
            ? <SidebarRow entry={entry} style={{ 'paddingLeft': `${props.level * 20}px` }} key={entry.cod} level={props.level + 1} />
            : <MenuItem item={entry} click={props.click} style={{ 'paddingLeft': `${props.level * 20}px` }} key={entry.cod} />
        )
        )}
      </div>
    </div>
  )
}

export default SidebarRow
