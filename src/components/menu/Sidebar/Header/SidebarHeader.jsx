import './SidebarHeader.css'
import React from 'react'
import { FaRegUserCircle } from 'react-icons/fa'

const SidebarHeader = props => {
  return (
    <div className="sidenavHeader">
      <FaRegUserCircle /> Bem-vindo, {props.username}
    </div>
  )
}

export default SidebarHeader
