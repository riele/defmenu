import './Sidebar.css'
import React, { useState, useEffect } from 'react'
// import { menuData } from '../../menuData'
import SidebarHeader from './Header/SidebarHeader'
import SidebarRow from './Row/SidebarRow'

const Sidebar = props => {
  const [entries, setEntries] = useState(null)

  useEffect(() => {
    fetch("http://localhost:3000/menu/1").then(data => data.json()).then(response => setEntries(response))
    // setEntries(menuData)
  }, [])

  return (
    <div className='sidebar'>
      <SidebarHeader username='João' />
      <div className='sidebarcontent'>
        {entries && (
          entries.map(entry => (
            <SidebarRow entry={entry} key={entry.cod} level={1} click={props.click} />
          ))
        )}
      </div>
    </div>
  )
}

export default Sidebar
